
export default function Navigation({handleLogOut}){
    return(
        <div style={styles.navigationContainer}>
            <h1>
                Auth Firebase
            </h1>
            <button style={{...styles.button,...styles.logoutButton}} onClick={()=>{handleLogOut()}}>
                Logout
            </button>
        </div>
    )
}


const styles={
    navigationContainer:{
        display:"flex",
        justifyContent:"space-between",
        alignItems:"center",
        height:"100px",
        paddingLeft:30,
        paddingRight:30
    },
    pageHeader:{
        textAlign:"center",
        marginBottom:20
    },
    button:{
        height:50,
        minWidth:100,
        padding:10,
        borderRadius:5,
        border:0,
        color:"white",
        cursor:"pointer"
    },
    logoutButton:{
         backgroundColor:"#ee8888",
    }
}