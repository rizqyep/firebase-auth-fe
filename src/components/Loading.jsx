export default function Loading(){
    return (
        <div style={styles.centeredContainer}>
            <h1 style={styles.loadingText}>Loading...</h1>
        </div>
    )
}

const styles ={
     centeredContainer:{
        display:"flex",
        justifyContent:"center",
        alignItems:"center",
        width:"100vw",
        height:"100vh"
    },
    loadingText:{
        textAlign:"center"
    }
}