import {getAuth, signInWithPopup, GoogleAuthProvider, onAuthStateChanged, signOut} from "firebase/auth";
import { useState } from "react";
import { useEffect } from "react";
import Navigation from "./components/Navigation";
import AuthPages from "./pages/AuthPages";
import TodoPages from "./pages/TodoPages";
import Loading from "./components/Loading";

function App() {

  const [auth,setAuth] = useState(false);
  const [token, setToken] = useState('');
  const [loading,setLoading] = useState(false);
  
  const firebaseAuth = getAuth();
  useEffect(()=>{
      onAuthStateChanged(firebaseAuth, (userCredential)=>{
        if(userCredential){
          setAuth(true);
          userCredential.getIdToken().then((token)=>{
            setToken(`GAUTH Bearer ${token}`);
          })
          setLoading(false);
        }
      })
  },[firebaseAuth])

  const loginWithGoogle = async ()=>{
    setLoading(true);
    console.log("login hit");
   
    const provider = new GoogleAuthProvider();
    signInWithPopup(firebaseAuth, provider)
      .then((result) => {
        const credential = GoogleAuthProvider.credentialFromResult(result);
        return credential;
      }).catch((error) => {
        console.log(error.message);
      });
  }

  const handleLogOut = async () =>{
    setLoading(true);
    const firebaseAuth = getAuth();

    try{
      await signOut(firebaseAuth);
      setAuth(false);
      setToken('');
      setTimeout( async ()=>{
         setLoading(false);
      },2000);
     
      
    }catch(err){
        alert(err.message);
    }
  }

  return (
    <div className="App">
      {
        loading ? (<Loading/>)
        : 
        auth ? 
        (
        <>        
          <Navigation handleLogOut={handleLogOut}/>
          <TodoPages token={token}/>
        </>
        ) : 
        (
          <AuthPages loginWithGoogle={loginWithGoogle}/>
        )
      }
      </div>
  );
}


const styles={
    centeredContainer:{
        display:"flex",
        justifyContent:"center",
        alignItems:"center",
        width:"100vw",
        height:"100vh"
    },
    pageHeader:{
        textAlign:"center",
        marginBottom:20
    },
    googleLoginButton:{
        backgroundColor:"#263238",
        height:60,
        minWidth:100,
        padding:20,
        borderRadius:5,
        border:0,
        color:"white",
        cursor:"pointer"
    },
    buttonContainer:{
        display:"flex",
        justifyContent:"center"
    }
}

export default App;
