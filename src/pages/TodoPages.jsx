import axios from "axios";
import { useEffect } from "react";

export default function TodoPages ({token}) {
    
    
    useEffect(()=>{
        if(token){
            fetchData();
        }
    })

    const fetchData = async ()=>{
        const {data} = await axios.get("http://localhost:3001/todos" ,{
            headers:{
                Authorization : `${token}`
            }
        }
        )
        const {data:todos} = data;
        console.log(todos);
    }

    return (
        <>        
            <h1>Todos</h1>
        </>

    );
}