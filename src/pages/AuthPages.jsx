export default function AuthPages({loginWithGoogle}){
    return (
        <div style={styles.centeredContainer}>     
            <div>   
                <h4 style={styles.pageHeader}>You are not yet authenticated</h4>
                <div style={styles.buttonContainer}>
                    <button style={styles.googleLoginButton} onClick={()=>{loginWithGoogle()}}>Login with Google</button> 
                </div>
            </div>
        </div>
    )
}

const styles={
    centeredContainer:{
        display:"flex",
        justifyContent:"center",
        alignItems:"center",
        width:"100vw",
        height:"100vh"
    },
    pageHeader:{
        textAlign:"center",
        marginBottom:20
    },
    googleLoginButton:{
        backgroundColor:"#263238",
        height:60,
        minWidth:100,
        padding:20,
        borderRadius:5,
        border:0,
        color:"white",
        cursor:"pointer"
    },
    buttonContainer:{
        display:"flex",
        justifyContent:"center"
    }
}